" .vimrc - vim configuration file
"   by alemedeiros <alexandre.n.medeiros _at_ gmail.com>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" General

" Enable Vim-only features
set nocompatible

"""""""""""""""""""""""""""""""""""""""
" Auto install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin()

"""""""""""""""""""
" Plugins

" File system tree
Plug 'scrooloose/nerdtree'

" Git integration
Plug 'tpope/vim-fugitive'

" A sweet and cool statusbar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" " General VCS gutter integration
" Plug 'mhinz/vim-signify'

" Git gutter integration
Plug 'airblade/vim-gitgutter'

" A shell plugin
Plug 'Shougo/vimproc.vim', { 'on': 'VimShell' }
Plug 'Shougo/vimshell.vim', { 'on': 'VimShell' }

" A syntatic checker system
Plug 'scrooloose/syntastic'

" Sane buffer deletion
Plug 'mhinz/vim-sayonara', { 'on': 'Sayonara' }

" Better csv
Plug 'chrisbra/csv.vim'

" Displays tags information on status bar
Plug 'majutsushi/tagbar'

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Plugin to comment stuff
Plug 'tpope/vim-commentary'

" Trailing whitespace highlighting
Plug 'ntpeters/vim-better-whitespace'

" Buffer explorer
Plug 'jlanzarotta/bufexplorer'

" Notes!
Plug 'xolox/vim-notes', { 'on': 'Note' }
Plug 'xolox/vim-misc'

" A fancy start screen
Plug 'mhinz/vim-startify'

" Colorschemes
Plug 'tomasr/molokai'

" YCM plugin
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}

" Haskell indentation and improved syntax highlight
" Plug 'neovimhaskell/haskell-vim'
Plug 'itchyny/vim-haskell-indent'

" Haskell Completion
Plug 'eagletmt/neco-ghc', { 'do': 'stack install ghc-mod' }

" Rainbow parentheses! :3
Plug 'luochen1990/rainbow'

" Eastwood (Clojure linter) integration
Plug 'venantius/vim-eastwood'

" Clojure REPL integration
Plug 'tpope/vim-fireplace'

" ***** maybe install later

" " Vim-Session
" Plug 'xolox/vim-session'

call plug#end()

filetype plugin indent on
"""""""""""""""""""""""""""""""""""""""
" Configuration

"" Basic Settings

" Set leader to ','
let mapleader=','

" Enable hidden buffers
set hidden

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Directories for swp files
set nobackup
set noswapfile

set visualbell t_vb=

" General tabs config
set smarttab
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

"" Visual Settings

set ruler
set number
set colorcolumn=81
set nowrap

colorscheme molokai
set background=dark

set mousemodel=popup
set t_Co=256
set cursorline

set gcr=a:blinkon0
set scrolloff=3

"" GUI options
" No menu please
let did_install_default_menus = 1
let did_install_syntax_menu = 1

set guifont=Roboto\ Mono\ for\ Powerline\ 10,Roboto\ Mono\ 10
set guiheadroom=0
set guioptions=Pci

"" Status bar
set laststatus=2

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif

"" Airline
let g:airline_theme = 'molokai'
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" Symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.paste     = 'Þ'
let g:airline_symbols.whitespace = 'Ξ'

" Mode strings
let g:airline_mode_map = {
      \ '__' : '- ',
      \ 'n'  : 'n ',
      \ 'i'  : 'i ',
      \ 'R'  : 'r ',
      \ 'c'  : 'c ',
      \ 'v'  : 'v ',
      \ 'V'  : 'vl',
      \ '^V' : 'vc',
      \ 's'  : 's ',
      \ 'S'  : 'sl',
      \ '^S' : 'sc',
      \ }

let g:airline#extensions#whitespace#trailing_format = '%s'
let g:airline#extensions#whitespace#mixed_indent_format = '%s'

let g:airline_exclude_preview = 0
set noshowmode

"" NERDTree configuration
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 50
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent> <F2> :NERDTreeFind<CR>
noremap <F3> :NERDTreeToggle<CR>

"" Vimshell
let g:vimshell_user_prompt = 'fnamemodify(getcwd(), ":~")'
let g:vimshell_prompt =  '$ '

" VimShell for vim and terminal for neovim
nnoremap <silent> <leader>sh :VimShell<CR>

"" Mappings

" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

" Git
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gsh :Gpush<CR>
noremap <Leader>gll :Gpull<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gb :Gblame<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>

" Notes
noremap <leader>n :Note<CR>

" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>x :bn<CR>

" Close buffer / panel
noremap <leader>c :Sayonara!<CR>
noremap <leader>q :close<CR>

" Clean search highlighting
nnoremap <silent> <leader><space> :noh<cr>

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

"" Ultisnips remapping
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"

"" Syntastic symbols
let g:syntastic_always_populate_loc_list=1
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_style_error_symbol = '✗'
let g:syntastic_style_warning_symbol = '⚠'
let g:syntastic_auto_loc_list=1
let g:syntastic_aggregate_errors = 1

"" neco-ghc
autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
let g:necoghc_enable_detailed_browse = 1
let g:ycm_semantic_triggers = {'haskell' : ['.', 're!\w+']}

"" Syntastic flags
let g:syntastic_cpp_cflags = '-std=c++11'

"" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" Configure Rainbow parentheses
let g:rainbow_active = 0
nmap <silent> <leader>r :RainbowToggle<CR>

"" LaTeX specific config
nmap  <leader>m  :make<CR>
autocmd FileType tex setlocal makeprg=latexmk\ -shell-escape\ -pdf\ %<

"" Toggle Paste-mode
command! -nargs=0 Paste call SetPasteToggle()
nmap <silent> <leader>p  :Paste<CR>

function! SetPasteToggle()
  if &paste == "1"
    set nopaste
  else
    set paste
  endif
  redraw
endfunction

syntax on
filetype plugin indent on
