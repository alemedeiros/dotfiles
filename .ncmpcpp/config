##### directories ######
ncmpcpp_directory = ~/.ncmpcpp
lyrics_directory = ~/.lyrics

##### connection settings #####
mpd_host = localhost
mpd_port = 6600
mpd_music_dir = ~/music
mpd_connection_timeout = 5

##### music visualizer #####
visualizer_fifo_path = /tmp/mpd.fifo
visualizer_output_name = Visualizer feed
visualizer_in_stereo = yes
visualizer_sync_interval = 30
visualizer_type = wave
visualizer_look = ◆│

##### delays #####
playlist_disable_highlight_delay = 5
message_delay_time = 5

##### song format #####
##
## for song format you can use:
##
## %l - length
## %f - filename
## %D - directory
## %a - artist
## %A - album artist
## %t - title
## %b - album
## %y - date
## %n - track number (01/12 -> 01)
## %N - full track info (01/12 -> 01/12)
## %g - genre
## %c - composer
## %p - performer
## %d - disc
## %C - comment
## %P - priority
## $R - begin right alignment
##
## you can also put them in { } and then it will be displayed
## only if all requested values are available and/or define alternate
## value with { }|{ } eg. {%a - %t}|{%f}
##
## Note: If you want to set limit on maximal length of a tag, just
## put the appropriate number between % and character that defines
## tag type, e.g. to make album take max. 20 terminal cells, use '%20b'.
##
## Note: Format that is similar to "%a - %t" (i.e. without any additional
## braces) is equal to "{%a - %t}", so if one of the tags is missing,
## you'll get nothing.
##
## text can also have different color than the main window has,
## eg. if you want length to be green, write "$3%l$9".
##
## Available values:
##
## - 0 - default window color (discards all other colors)
## - 1 - black
## - 2 - red
## - 3 - green
## - 4 - yellow
## - 5 - blue
## - 6 - magenta
## - 7 - cyan
## - 8 - white
## - 9 - end of current color
##
## Note: colors can be nested.
##

#song_list_format = {%a - }{%t}|{$8%f$9}$R{$3(%l)$9}

#song_status_format = {{%a{ "%b"{ (%y)}} - }{%t}}|{%f}

#song_library_format = {%n - }{%t}|{%f}

#tag_editor_album_format = {(%y) }%b

##
## Note: Below variables are used for sorting songs in browser.
## The sort mode determines how songs are sorted, and can be used
## in combination with a sort format to specify a custom sorting format.
## Available values for browser_sort_mode are "name", "mtime", "format"
## and "noop".
##

#browser_sort_mode = name

#browser_sort_format = {%a - }{%t}|{%f} {(%l)}

##
## Note: Below variables are for alternative version of user's interface.
## Their syntax supports all tags and colors listed above plus some extra
## markers used for text attributes. They are followed by character '$'.
## After that you can put:
##
## - b - bold text
## - u - underline text
## - r - reverse colors
## - a - use alternative character set
##
## If you don't want to use an attribute anymore, just put it again, but
## this time insert character '/' between '$' and attribute character,
## e.g. {$b%t$/b}|{$r%f$/r} will display bolded title tag or filename
## with reversed colors.
##

#alternative_header_first_line_format = $b$1$aqqu$/a$9 {%t}|{%f} $1$atqq$/a$9$/b

#alternative_header_second_line_format = {{$4$b%a$/b$9}{ - $7%b$9}{ ($4%y$9)}}|{%D}

##
## Note: below variables also support text attributes listed above.
##

#now_playing_prefix = $b

#now_playing_suffix = $/b

#browser_playlist_prefix = "$2playlist$9 "

#selected_item_prefix = $6

#selected_item_suffix = $9

#modified_item_prefix = $3> $9

## Note: colors are not supported for below variable.
##
#song_window_title_format = {%a - }{%t}|{%f}

##### columns settings #####
##
## syntax of song columns list format is "column column etc."
##
## - syntax for each column is:
##
## (width of column)[column's color]{displayed tag}
##
## Note: Width is by default in %, if you want a column to
## have fixed size, add 'f' after the value, e.g. (10)[white]{a}
## will be the column that take 10% of screen (so the real column's
## width will depend on actual screen size), whereas (10f)[white]{a}
## will take 10 terminal cells, no matter how wide the screen is.
##
## - color is optional (if you want the default one, type [])
##
## Note: You can give a column additional attributes by putting appropriate
## character after displayed tag character. Available attributes are:
##
## - r - column will be right aligned
## - E - if tag is empty, empty tag marker won't be displayed
##
## You can also:
##
## - give a column custom name by putting it after attributes,
##   separated with character ':', e.g. {lr:Length} gives you
##   right aligned column of lengths named "Length".
##
## - define sequence of tags, that have to be displayed in case
##   predecessor is empty in a way similar to the one in classic
##   song format, i.e. using '|' character, e.g. {a|c|p:Owner}
##   creates column named "Owner" that tries to display artist
##   tag and then composer and performer if previous ones are
##   not available.
##

#song_columns_list_format = (20)[]{a} (6f)[green]{NE} (50)[white]{t|f:Title} (20)[cyan]{b} (7f)[magenta]{l}

##### various settings #####
#execute_on_song_change = ""
playlist_show_remaining_time = yes
playlist_shorten_total_times = yes
playlist_separate_albums = yes
playlist_display_mode = columns
browser_display_mode = columns
search_engine_display_mode = columns
playlist_editor_display_mode = classic
discard_colors_if_item_is_selected = yes
incremental_seeking = yes
seek_time = 1
volume_change_step = 2
autocenter_mode = yes
centered_cursor = yes
progressbar_look = =].
progressbar_boldness = yes
default_place_to_search_in = database
user_interface = alternative
data_fetching_delay = yes
media_library_primary_tag = artist
default_find_mode = wrapped
default_space_mode = add
default_tag_editor_pattern = %n - %t
header_visibility = yes
statusbar_visibility = yes
titles_visibility = yes
header_text_scrolling = yes
cyclic_scrolling = yes
lines_scrolled = 2
follow_now_playing_lyrics = no
fetch_lyrics_for_current_song_in_background = yes
store_lyrics_in_song_dir = no
generate_win32_compatible_filenames = no
allow_for_physical_item_deletion = no
lastfm_preferred_language = en
space_add_mode = always_add
show_hidden_files_in_local_browser = no
screen_switcher_mode = playlist, media_library, visualizer
startup_screen = playlist
locked_screen_width_part = 50
ask_for_locked_screen_width_part = yes
jump_to_now_playing_song_at_start = yes
ask_before_clearing_playlists = yes
clock_display_seconds = no
display_volume_level = yes
display_bitrate = yes
display_remaining_time = yes
regular_expressions = basic
ignore_leading_the = yes
block_search_constraints_change_if_items_found = yes
mouse_support = yes
mouse_list_scroll_whole_page = yes
empty_tag_marker = ε
tags_separator = " | "
tag_editor_extended_numeration = no
media_library_sort_by_mtime = no
enable_window_title = yes
search_engine_default_search_mode = 2
external_editor = vim
use_console_editor = yes

##### colors definitions #####
colors_enabled = yes
empty_tag_color = cyan
header_window_color = default
volume_color = default
state_line_color = default
state_flags_color = default
main_window_color = yellow
color1 = white
color2 = green
main_window_highlight_color = yellow
progressbar_color = black
progressbar_elapsed_color = green
statusbar_color = default
alternative_ui_separator_color = black
active_column_color = red
visualizer_color = yellow
window_border_color = green
active_window_border = red

