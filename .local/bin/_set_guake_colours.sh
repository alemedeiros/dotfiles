#!/bin/bash 

gconftool-2 -s -t string /apps/guake/style/background/color '#2d2d2d2d2d2d'
gconftool-2 -s -t string /apps/guake/style/font/color '#dededededede'
gconftool-2 -s -t string /apps/guake/style/font/palette '#333333333333:#d6d649493737:#9797e1e12323:#dfdfd4d46060:#0f0f7f7fcfcf:#87870000ffff:#4242a7a7cfcf:#bbbbbbbbbbbb:#555555555555:#f5f566669c9c:#b0b0e0e05e5e:#fefef2f26c6c:#0000afafffff:#afaf8787ffff:#5050cdcdfefe:#f9f9f9f9f9f9'
