;; TODO Use env vars set on Xprofile for customization between computers

[colors]
;; If colors are changed, check network modules and xmonad config
accent = #1a73e8
background = #1f1f1f
foreground = #dedede
foreground-alt = #616161
border = #424242

;; TODO review these colors
urgent = #ff0000
alert = #ff0000

[bar/primary]
width = 100%
height = 42
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3

border-bottom-size = 3
border-color = ${colors.border}

padding = 1
module-margin = 1

font-0 = Fira Code:style=Regular:size=9:antialias=true;6
font-1 = Noto Sans:style=Regular:size=9:antialias=true;6
font-2 = Symbols Nerd Font:size=12:antialias=true;6
font-3 = Noto Color Emoji:size=12:antialias=true;6

modules-left = arch-logo xmonad-log
modules-center = 
modules-right = github ethernet cpu memory temperature pulseaudio datetime

separator = :

tray-position = center
tray-detached = false
tray-maxsize = 32
tray-padding = 0
tray-scale = 1.0

dpi = 0

;; TODO consider default bar actions
; click-left = 
; click-middle = 
; click-right =
; scroll-up =
; scroll-down =
; double-click-left =
; double-click-middle =
; double-click-right =

cursor-click = pointer
cursor-scroll = ns-resize

[settings]
screenchange-reload = true

;; TODO check what we can do with compositing
;; https://www.cairographics.org/manual/cairo-cairo-t.html#cairo-operator-t
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 0
margin-bottom = 0

;; Modules

; TODO check:
;  - github
;  - menu
;  - xkeyboard (for indicators)
;
;  - laptop only:
;    - backlight OR xbacklight
;    - battery
;    - network (for wifi)
;    - scripts (for vpn)
;
;  - maybe give a chance sometime
;    - polybar-scripts/info-hackspeed
;    - polybar-scripts/update-arch-combined
;    - polybar-scripts/player-mpris-tail
;    - polybar-scripts/system-bluetooth-bluetoothctl

[module/cpu]
type = internal/cpu
interval = 0.5

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-overline = ${colors.foreground-alt}
label = %percentage:2%%

[module/datetime]
type = internal/date
interval = 1

date = "%F"
time = "%I:%M%P"

;; TODO consider a more complex alt date-time
date-alt = "(%A) %d %B %Y"
time-alt = "%I:%M:%S%P"

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-overline = ${colors.accent}

label = %date% %time%

[module/memory]
type = internal/memory
interval = 0.5

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-overline = ${colors.foreground-alt}
label = %percentage_used:2%%

[module/ethernet]
type = internal/network
interval = 0.5

;; TODO get interface name from ~~MAGIC~~ place
interface = enp0s31f6

format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
format-connected-overline = ${colors.foreground-alt}

;; TODO as-of 2020-04-05 we can't use variables inside label tags
;; TODO consider using different color for down/upload icons
label-connected = %{F#616161}%{F-} %downspeed:8% %{F#616161}祝%{F-} %upspeed:8%

format-disconnected-prefix = " "
format-disconnected-prefix-foreground = ${colors.foreground-alt}
format-disconnected-overline = ${colors.alert}

label-disconnected =  disconnected
label-disconnected-foreground = ${colors.foreground-alt}

;; TODO on laptop show nothing on disconnected

[module/github]
type = internal/github
interval = 30

token = ${env:POLYBAR_GH_TOKEN}
user = alemedeiros
empty-notifications = false

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-overline = ${colors.accent}

label = %notifications%

format-offline =  年

[module/pulseaudio]
type = internal/pulseaudio
interval = 1

;; TODO get sink dynamically
; sink = ??
use-ui-max = false

;; TODO check when this feature is released
; click-right = pavucontrol &

format-volume = <ramp-volume> <label-volume>
format-volume-overline = ${colors.foreground-alt}
ramp-volume-foreground = ${colors.foreground-alt}
ramp-volume-0 = 奄
ramp-volume-1 = 奔
ramp-volume-2 = 墳

label-volume = %percentage:2%%

format-muted = ﱝ <label-muted>
format-muted-overline = ${colors.foreground-alt}
format-muted-foreground = ${colors.foreground-alt}

label-muted = %percentage:2%%
label-muted-foreground = ${colors.foreground-alt}

[module/temperature]
type = internal/temperature
interval = 0.5

hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
base-temperature = 40
warn-temperature = 80

format = <ramp> <label>
format-overline = ${colors.foreground-alt}

format-warn = <ramp> <label-warn>
format-warn-overline = ${colors.alert}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[module/xmonad-log]
type = custom/script

exec-if = [ -p /tmp/.xmonad-log ]
exec = tail -F /tmp/.xmonad-log
tail = true

[module/arch-logo]
type = custom/text

content = 
content-foreground = ${colors.accent}

; vim:ft=dosini
