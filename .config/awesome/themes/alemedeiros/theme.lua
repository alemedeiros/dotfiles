-------------------------------
-- alemedeiros awesome theme --
-------------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gears = require("gears")
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local this_theme_path = gfs.get_configuration_dir() .. "themes/alemedeiros/"

local theme = dofile(themes_path .. "default/theme.lua")

-- {{ colors table (inspired from numix)
theme.colors = {}

-- theme.colors.red          = "#d64937ff" -- old accent?
theme.colors.red            = "#f0544cff"

theme.colors.white_1        = "#eeeeeeff" -- general foreground
theme.colors.white_2        = "#b3b3b3ff" -- foreground (unfocused)
theme.colors.white_3        = "#ddddddff" -- foreground (titlebar)
theme.colors.true_white     = "#ffffffff" -- real white

theme.colors.light_grey_1   = "#f0f0f0ff" -- background of buttons / widget (active)
theme.colors.light_grey_2   = "#d3d3d3ff" -- background of buttons / widget (inactive)
theme.colors.light_grey_3   = "#b2b2b2ff" -- background of buttons / widget (pressed)
theme.colors.light_grey_4   = "#dededeff" -- main general background
theme.colors.light_grey_5   = "#efefefff" -- secondary general background

theme.colors.grey_1         = "#707070ff" -- background (selected / hovering)
theme.colors.grey_2         = "#9a9a9aff" -- foreground of buttons / widget (inactive)
theme.colors.grey_3         = "#aaaaaaff" -- foreground (titlebar unfocused)
theme.colors.grey_4         = "#c8c8c8ff" -- border for light_grey_5 (#efefef)

theme.colors.dark_grey_1    = "#444444ff" -- general background
theme.colors.dark_grey_2    = "#555555ff" -- foreground of buttons / widget (active)
theme.colors.dark_grey_3    = "#525252ff" -- border for black_3 (#333333) on dark colors (widgets)
theme.colors.dark_grey_4    = "#5c5c5cff" -- (unfocused) foreground on light_grey_5 (#efefef)

theme.colors.black_1        = "#303030ff" -- borders (tooltip + most shit)
theme.colors.black_2        = "#222222ff" -- borders (widgets)
theme.colors.black_3        = "#333333ff" -- input fields backgroung and (focused) foreground on light_grey_5 (#efefef)
theme.colors.black_4        = "#2a2a2aff" -- border of shit inside a black_3 (#333333)

-- pre fixes -- TODO REMOVE
theme.colors.grey         = "#3d3d3dff"
theme.colors.dark_grey    = "#2d2d2dff"
-- }}

-- accent color
theme.accent       = theme.colors.red

-- font
theme.font          = "RobotoMono Nerd Font 9"

-- background colors
theme.bg_normal     = theme.colors.dark_grey_1
theme.bg_focus      = theme.colors.dark_grey_1
theme.bg_urgent     = theme.accent
theme.bg_minimize   = theme.colors.black_4 -- TODO use text / effect instead of color
theme.bg_systray    = theme.bg_normal

-- foreground colors
theme.fg_normal     = theme.colors.white_2
theme.fg_focus      = theme.colors.white_1
theme.fg_urgent     = theme.colors.true_white
theme.fg_minimize   = theme.fg_normal

-- border and gap
theme.useless_gap   = dpi(3)
theme.border_width  = dpi(2)
theme.border_normal = theme.colors.black_1
theme.border_focus  = theme.accent
theme.border_marked = theme.border_normal -- TODO find out what marked is :shrug:

-- taglist TODO
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
local taglist_square_size = dpi(5)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- tasklist TODO
-- tasklist_[bg|fg]_[focus|urgent]
theme.tasklist_disable_icon = true

-- titlebar TODO
-- titlebar_[bg|fg]_[normal|focus]
theme.titlebar_height = theme.menu_height

-- tooltip TODO
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]

-- mouse_finder TODO
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]

-- prompt TODO
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
theme.prompt_bg = theme.colors.black_3
theme.prompt_bg_cursor = theme.accent

-- hotkeys TODO
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]

-- notification
theme.notification_font = "Roboto 10"
theme.notification_bg = theme.bg_normal
theme.notification_fg = theme.fg_normal
theme.notification_width = 300
theme.notification_height = 80
theme.notification_max_width = 500
theme.notification_max_height = 80
theme.notification_icon_size = 80
theme.notification_border_color = theme.colors.black_1
theme.notification_border_width = dpi(2)
theme.notification_shape = gears.shape.rectangle

-- menu
theme.menu_submenu_icon = themes_path .. "default/submenu.png" -- TODO
theme.menu_height       = dpi(20)
theme.menu_width        = dpi(200)
theme.menu_border_width = dpi(1)
theme.menu_border_color = theme.colors.black_1

-- menubar TODO
theme.menubar_bg_normal = theme.colors.black_3
theme.menubar_height = theme.menu_height

-- wibar
theme.wibar_height = theme.menu_height

-- {{ icons and images
-- wallpaper
theme.wallpaper = function (s)
    return theme_assets.wallpaper(theme.colors.black_2, theme.fg_normal, theme.accent, s)
end

-- awesome icon
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.accent, theme.colors.black_3)

-- recolor layout icons
theme = theme_assets.recolor_layout(theme, theme.fg_normal)

-- recolor titlebar icons TODO
theme = theme_assets.recolor_titlebar(theme, theme.fg_normal, "normal")
theme = theme_assets.recolor_titlebar(theme, theme.colors.dark_grey, "normal", "hover")
theme = theme_assets.recolor_titlebar(theme, theme.accent, "normal", "press")
theme = theme_assets.recolor_titlebar(theme, theme.fg_focus, "focus")
theme = theme_assets.recolor_titlebar(theme, theme.colors.grey, "focus", "hover")
theme = theme_assets.recolor_titlebar(theme, theme.accent, "focus", "press")

-- use default icon theme
theme.icon_theme = nil
-- }}

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
