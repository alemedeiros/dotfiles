"""""""""""""""""""""""""""""""""""""""
" General Configuration


"" Basic Settings
filetype plugin indent on
syntax on
set hidden

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Directories and files
set nobackup
set noswapfile
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite

" General tabs config
set smarttab
set tabstop=2
set softtabstop=0
set shiftwidth=2
set expandtab

" Increcmental command
set inccommand=split

"" Visual Settings
set ruler
set number
set relativenumber
set colorcolumn=81
set nowrap

set background=dark
colorscheme PaperColor

set mousemodel=popup
set cursorline

set gcr=a:blinkon0
set scrolloff=3

"" Status bar
set noshowmode
set laststatus=2
set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif
" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"" Toggle Paste-mode
command! -nargs=0 Paste call SetPasteToggle()
nmap <silent> <leader>p :Paste<CR>

function! SetPasteToggle()
  if &paste == "1"
    set nopaste
  else
    set paste
  endif
  redraw
endfunction

"" LaTeX specific config
autocmd FileType tex setlocal makeprg=latexmk\ -shell-escape\ -pdf\ %<
