" init.vim - neovim configuration file
"   by alemedeiros <alexandre.n.medeiros _at_ gmail.com>

" Plugins installation
source $HOME/.config/nvim/plugins.vim

" General configuration
source $HOME/.config/nvim/config.vim

" Plugins configuration
source $HOME/.config/nvim/plugin-config.vim

" Keymapping
source $HOME/.config/nvim/keymapping.vim
