"""""""""""""""""""""""""""""""""""""""
" Plugins Installation


"""""""""""""""""""""""""""""""""""""""
" Auto install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin()


"""""""""""""""""""
" General Plugins

" File system tree
Plug 'scrooloose/nerdtree'
" Buffer explorer
Plug 'jlanzarotta/bufexplorer'
" Better comment commands
Plug 'tpope/vim-commentary'
" fzf plugin
Plug 'junegunn/fzf.vim'
" Sane buffer deletion
Plug 'mhinz/vim-sayonara', { 'on': 'Sayonara' }
" Code Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Make
Plug 'neomake/neomake'
" rainbow parentheses! :3
Plug 'luochen1990/rainbow'

" misc xolox functions
Plug 'xolox/vim-misc', { 'for': 'lua' }


"""""""""""""""""""
" temp clojure plugins editing

" clojure - language plugin
" Plug 'clojure-vim/acid.nvim', { 'do': ':UpdateRemotePlugins' }

" clojure - REPL integration
" Plug 'hkupty/iron.nvim', { 'for': 'clojure', 'do': ':UpdateRemotePlugins' }


"""""""""""""""""""
" Git/VCS Plugins

" Git integration
Plug 'tpope/vim-fugitive'
" Git gutter integration
Plug 'airblade/vim-gitgutter'
" " Generic VCS gutter integration
" Plug 'mhinz/vim-signify'


"""""""""""""""""""
" Eye-candy Plugins

" A sweet and cool statusbar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Colorschemes
Plug 'tomasr/molokai'
Plug 'NLKNguyen/papercolor-theme'
" TODO: configure this plugin properly or don't use it
" " A fancy start screen
" Plug 'mhinz/vim-startify'


"""""""""""""""""""
" Filetype specific plugins

" csv - better csv
Plug 'chrisbra/csv.vim', { 'for': 'csv' }

" haskell - indentation and improved syntax highlight
Plug 'neovimhaskell/haskell-vim', { 'for': 'haskell' }

" python - Completion
Plug 'zchee/deoplete-jedi', { 'for': 'python' }

" vim - Completion
Plug 'shougo/neco-vim', { 'for': 'vim' }

" kotlin - filetype
Plug 'udalov/kotlin-vim', { 'for': 'kotlin' }

" lua - Completion
Plug 'xolox/vim-lua-ftplugin', { 'for': 'lua' }

"""""""""""""""""""
" TODO: check if we really need these

" " Displays tags information on status bar
" Plug 'majutsushi/tagbar'
" " Snippets
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" " Trailing whitespace highlighting
" Plug 'ntpeters/vim-better-whitespace'

" Snippets ??
" Plug 'shougo/neosnippet.vim'
" Plug 'shougo/neosnippet-snippets'

call plug#end()
