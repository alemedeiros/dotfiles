### oh-my-zsh configuration

ZSH=/usr/share/oh-my-zsh/
ZSH_CUSTOM=$HOME/.config/zsh-custom
DISABLE_AUTO_UPDATE="true"
HIST_STAMPS="yyyy-mm-dd"

if [[ ! -d "$ZSH_CUSTOM/themes/powerlevel10k" ]]; then
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "$ZSH_CUSTOM/themes/powerlevel10k"
fi
ZSH_THEME="powerlevel10k/powerlevel10k"

HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(
  ## plugins
  aws
  jump
  vi-mode
  fzf

  ## aliases
  archlinux
  systemd
  git
  tmux

  ## sudo plugin must be last
  sudo
)

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

### start oh-my-zsh
source $ZSH/oh-my-zsh.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

### personal configuration
export LANG=en_US.utf8

# Default programs
if [[ -x `command -v nvim` ]]; then
  export EDITOR="nvim"
elif [[ -x `command -v vim` ]]; then
  export EDITOR="vim"
elif [[ -x `command -v vi` ]]; then
  export EDITOR="vi"
fi
if [[ -x `command -v brave` ]]; then
  export BROWSER="brave"
elif [[ -x `command -v chromium` ]]; then
  export BROWSER="chromium"
elif [[ -x `command -v chromium-browser` ]]; then
  export BROWSER="chromium-browser"
elif [[ -x `command -v google-chrome` ]]; then
  export BROWSER="google-chrome"
elif [[ -x `command -v firefox` ]]; then
  export BROWSER="firefox"
fi

# history config
HISTSIZE=100000
SAVEHIST=400000

# Set paths
export PATH=$PATH:$HOME/.local/bin
if [[ -z $LD_LIBRARY_PATH  ]]; then
  export LD_LIBRARY_PATH=$HOME/.lib
else
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/.lib
fi
if [[ -z $LIBRARY_PATH  ]]; then
  export LIBRARY_PATH=$HOME/.lib
else
  export LIBRARY_PATH=$LIBRARY_PATH:$HOME/.lib
fi

# GnuPG config
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

# Load nubank dev configurations
[[ -f $HOME/.nurc ]] && source $HOME/.nurc

# load secrets
[[ -f $HOME/.secretsrc ]] && source $HOME/.secretsrc

### aliases

# alias grep='grep --color=auto'
# alias info='info --vi-keys'
# alias mkdir='mkdir -p -v'
# alias mv='mv -i'
# alias cp='cp -i'
# alias :q='exit'
# alias tmux='tmux -2'
# alias ta='tmux attach -t'
# alias tls='tmux ls'
# alias tn='tmux new -s'
alias zshconfig="$EDITOR $HOME/.zshrc"
alias fa='alias | ag '

